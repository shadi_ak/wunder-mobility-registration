Mobilityfleet Registration App Test Task 
================

### How to set up the app? ###

1. After pulling the code, run `composer install` to get all the 3rd-party libraries.
2. Create a new database and import the schema to it from `data/registration.sql`.
3. Copy `.env.example` into `.env` and enter your database connection string and the payment API URL in it.

### Describe possible performance optimizations for your Code ###
I cannot think of something here. The app is not memory intensive, and it should have a good performance.
The only 2 actions that will consume a relativily bigger amount of time are: when hitting the database for insert and update, and when sending a request to the external payment API, and these 2 actions cannot be done asynchronously.


### Which things could be done better? ###

There are some things that I wanted to improve/add, but could not due to the limited time.
For example:

* Sending Ajax requests when going to the next step for smoother UI.
* Error handling can be improved a bit.
* Proper Logging.
* Test classes.
* Abstract the database connection part to enable switching between different drivers.
* All the session handling happens now in the Controllers (Presenter), which is maybe not the best since the sessions can be seen as state of the app, and therefore they belong to the Model. But it works fine for me like this and I did not have time to rewrite that.

### Note about the structure ###
The app follows MVP pattern. The View is passive and unaware of the Model.
I find that following MVP (or MVC) when writing a web application make it much easier to extend and maintain and understand due to the decoupling between the application's parts and the separation of concerns.