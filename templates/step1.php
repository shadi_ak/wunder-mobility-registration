    <div id="header">Personal Information</div>
    <?php if($vars['error']): ?>
    <span class="error-message">Something wrong happened! please try again.</span>
    <?php endif; ?>
    <div id="content">
        <form method="post">
            <div class="row">
                <div class="col"><label for="firstname">First Name: </label></div>
                <div class="col"><input type="text" name="firstname" required="required" /></div>
            </div>
            <div class="row">
                <div class="col"><label for="lastname">Last Name: </label></div>
                <div class="col"><input type="text" name="lastname" required="required" /></div>
            </div>
            <div class="row">
                <div class="col"><label for="telephone">Telephone Number: </label></div>
                <div class="col"><input type="tel" name="telephone" required="required" /></div>
            </div>
            <div class="row">
                <div class="col"><input type="submit" name="submit" value="Next" /></div>
            </div>
        </form>
    </div>