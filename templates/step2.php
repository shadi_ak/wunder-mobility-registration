    <div id="header">Address Information</div>
    <?php if($vars['error']): ?>
    <span class="error-message">Something wrong happened! please try again.</span>
    <?php endif; ?>
    <div id="content">
        <form method="post">
            <div class="row">
                <div class="col"><label for="street">Street: </label></div>
                <div class="col"><input type="text" name="street" required="required" /></div>
            </div>
            <div class="row">
                <div class="col"><label for="houseNumber">House Number: </label></div>
                <div class="col"><input type="text" name="houseNumber" required="required" /></div>
            </div>
            <div class="row">
                <div class="col"><label for="zipCode">zip code: </label></div>
                <div class="col"><input type="text" name="zipCode" required="required" /></div>
            </div>
            <div class="row">
                <div class="col"><label for="city">City: </label></div>
                <div class="col"><input type="text" name="city" required="required" /></div>
            </div>
            <div class="row">
                <div class="col"><input type="submit" name="submit" value="Next" /></div>
            </div>
        </form>
    </div>