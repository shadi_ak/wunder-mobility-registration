    <div id="header">Payment Information</div>
    <?php if($vars['error']): ?>
    <span class="error-message">Something wrong happened! please try again.</span>
    <?php endif; ?>
    <div id="content">
        <form method="post">
            <div class="row">
                <div class="col"><label for="accountOwner">Account Owner: </label></div>
                <div class="col"><input type="text" name="accountOwner" required="required" /></div>
            </div>
            <div class="row">
                <div class="col"><label for="iban">IBAN: </label></div>
                <div class="col"><input type="text" name="iban" required="required" /></div>
            </div>
            <div class="row">
                <div class="col"><input type="submit" name="submit" value="Submit" /></div>
            </div>
        </form>
    </div>