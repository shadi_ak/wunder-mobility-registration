<?php

/**
 * The User Model
 *
 * @author shadi
 */

namespace BasicRegistrationApp\Model;

use BasicRegistrationApp\Util\DBConnection;

class User {

    private $id;
    private $firstname;
    private $lastname;
    private $telephone;
    private $street;
    private $houseNumber;
    private $zipCode;
    private $city;
    private $accountOwner;
    private $iban;
    private $paymentDataId;
    private $conn;

    function __construct($firstname, $lastname, $telephone, $street, $houseNumber, $zipCode, $city, $accountOwner, $iban) {
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->telephone = $telephone;
        $this->street = $street;
        $this->houseNumber = $houseNumber;
        $this->zipCode = $zipCode;
        $this->city = $city;
        $this->accountOwner = $accountOwner;
        $this->iban = $iban;

        $this->conn = DBConnection::getInstance()->getConnection();
    }

    /**
     * Stores all the user's data in the database except the $paymentDataId
     */
    public function save() {
        $sql = "INSERT INTO User (firstname, lastname, telephone, street, house_number, zip_code, city, account_owner, iban) VALUES (?,?,?,?,?,?,?,?,?)";
        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute([$this->firstname, $this->lastname, $this->telephone, $this->street, $this->houseNumber, $this->zipCode, $this->city, $this->accountOwner, $this->iban]) > 0) {
            $this->id = $this->conn->lastInsertId();
            return true;
        } else {
            echo false;
        }
    }

    public function savePaymentDataId($paymentDataId) {
        if ($this->id !== null) {
            $sql = "UPDATE User SET payment_data_id=? WHERE id=?";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute([$paymentDataId, $this->id]);
            $this->paymentDataId = $paymentDataId;
        }
    }
    
    function getId() {
        return $this->id;
    }

    function getFirstname() {
        return $this->firstname;
    }

    function getLastname() {
        return $this->lastname;
    }

    function getTelephone() {
        return $this->telephone;
    }

    function getStreet() {
        return $this->street;
    }

    function getHouseNumber() {
        return $this->houseNumber;
    }

    function getZipCode() {
        return $this->zipCode;
    }

    function getCity() {
        return $this->city;
    }

    function getAccountOwner() {
        return $this->accountOwner;
    }

    function getIban() {
        return $this->iban;
    }

    function getPaymentDataId() {
        return $this->paymentDataId;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setFirstname($firstname) {
        $this->firstname = $firstname;
    }

    function setLastname($lastname) {
        $this->lastname = $lastname;
    }

    function setTelephone($telephone) {
        $this->telephone = $telephone;
    }

    function setStreet($street) {
        $this->street = $street;
    }

    function setHouseNumber($houseNumber) {
        $this->houseNumber = $houseNumber;
    }

    function setZipCode($zipCode) {
        $this->zipCode = $zipCode;
    }

    function setCity($city) {
        $this->city = $city;
    }

    function setAccountOwner($accountOwner) {
        $this->accountOwner = $accountOwner;
    }

    function setIban($iban) {
        $this->iban = $iban;
    }

    function setPaymentDataId($paymentDataId) {
        $this->paymentDataId = $paymentDataId;
    }
}
