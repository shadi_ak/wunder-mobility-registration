<?php

/**
 * Description of User
 *
 * @author shadi
 */

namespace BasicRegistrationApp\Util;

use PDO;

class DBConnection {

    private static $instance = null;
    private $conn;
    private $host;
    private $user;
    private $password;
    private $dbname;

    private function __construct() {
        $this->host = $_ENV['DATABASE_HOST'];
        $this->user = $_ENV['DATABASE_USER'];
        $this->password = $_ENV['DATABASE_PASSWORD'];
        $this->dbname = $_ENV['DATABASE_DATABASE'];
        $this->conn = new PDO("mysql:host={$this->host};dbname={$this->dbname}", $this->user, $this->password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
    }

    public static function getInstance() {
        if (!self::$instance) {
            self::$instance = new DBConnection();
        }

        return self::$instance;
    }

    public function getConnection() {
        return $this->conn;
    }

}
