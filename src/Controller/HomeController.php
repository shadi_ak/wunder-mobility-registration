<?php

namespace BasicRegistrationApp\Controller;

use  Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * HomeController is just for redirecting the visitor to the registration page.
 *
 * @author shadi
 */
class HomeController {
    public function index(): RedirectResponse {
        $response = new RedirectResponse('/registration');
        return $response;
    }
}