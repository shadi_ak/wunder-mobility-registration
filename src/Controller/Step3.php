<?php

namespace BasicRegistrationApp\Controller;

use Symfony\Component\HttpFoundation\Request;
use BasicRegistrationApp\Model\User;

/**
 * Description of Step3
 *
 * @author shadi
 */
class Step3 extends AbstractStep {

    protected $request;
    private $user;
    protected $fields = array('accountOwner', 'iban');

    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function process() {
        $session = $this->request->getSession();
        if ($this->fieldsHasData()) {
            foreach ($this->fields as $field) {
                $session->set($field, htmlspecialchars($this->request->request->get($field)));
            }
            if ($this->storeUserData()) {
                $paymentDataId = $this->sendPaymentRequest($this->user->getId(), $this->user->getIban(), $this->user->getAccountOwner());
                if ($paymentDataId !== FALSE) {
                    $this->user->savePaymentDataId($paymentDataId);
                    $session->set('paymentDataId', $paymentDataId);
                    $session->set('success', '1');
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function storeUserData() {
        $session = $this->request->getSession();
        $this->user = new User(
                $session->get('firstname'), $session->get('lastname'), $session->get('telephone'), $session->get('street'), $session->get('houseNumber'), $session->get('zipCode'), $session->get('city'), $session->get('accountOwner'), $session->get('iban')
        );
        return $this->user->save();
    }

    private function sendPaymentRequest($userId, $iban, $accountOwner) {
        if ($_ENV['PAYMENT_API_URL'] !== null) {
            $payload = array("customerId" => $userId, "iban" => $iban, "owner" => $accountOwner);
            $payloadJSON = json_encode($payload);

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $_ENV['PAYMENT_API_URL']);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($curl, CURLOPT_POST, TRUE);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $payloadJSON);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($payloadJSON))
            );
            $paymentAPIResponse = curl_exec($curl);
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if ($httpCode === 200) {
                $paymentAPIResponseArray = json_decode($paymentAPIResponse, true);
                return $paymentAPIResponseArray['paymentDataId'];
            } else {
                return false;
            }
            curl_close($curl);
        } else {
            return false;
        }
    }

}
