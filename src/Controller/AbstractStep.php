<?php

namespace BasicRegistrationApp\Controller;

/**
 * Description of AbstractStep
 *
 * @author shadi
 */
class AbstractStep {
    
    protected $request;
    protected $fields;
    
    protected function fieldsHasData() {
        foreach ($this->fields as $field) {
            if ($this->request->request->get($field) === null || trim($this->request->request->get($field)) === '') {
                return false;
            }
            return true;
        }
    }
}
