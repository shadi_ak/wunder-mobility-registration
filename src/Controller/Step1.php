<?php

namespace BasicRegistrationApp\Controller;

use Symfony\Component\HttpFoundation\Request;

/**
 * Description of Step1
 *
 * @author shadi
 */
class Step1 extends AbstractStep{

    protected $request;
    protected $fields = array('firstname', 'lastname', 'telephone');

    function __construct(Request $request) {
        $this->request = $request;
    }

    public function process() {
        $session = $this->request->getSession();
        if ($this->fieldsHasData()) {
            foreach ($this->fields as $field) {
                $session->set($field, htmlspecialchars($this->request->request->get($field)));
            }
            $session->set('step', '2');
        } else {
            return false;
        }
    }
}
