<?php

namespace BasicRegistrationApp\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use BasicRegistrationApp\View\View;

/**
 * SuccessController will handle the request after all the steps are done, the data is stored in the database, and the payment API is called.
 *
 * @author shadi
 */
class SuccessController {

    private $request;
    private $view;

    public function __construct(Request $request, View $view) {
        $this->request = $request;
        $this->view = $view;
    }

    public function index() {
        $session = $this->request->getSession();
        if ($session->get('success') !== null && $session->get('paymentDataId') !== null) {
            $html = $this->view->render('success.php', array("paymentDataId" => $session->get('paymentDataId')));
            $session->invalidate(); // clear all sessions
            return new Response($html);
        } else {
            $response = new RedirectResponse('/registration');
            return $response;
        }
    }

}
