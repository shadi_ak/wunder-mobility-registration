<?php

namespace BasicRegistrationApp\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use BasicRegistrationApp\View\View;

/**
 * RegistrationController is responsible for handling all the registration steps.
 * The current step is stored in a session called 'step'. This session value is used to know in which step the customer is now.
 * Depending on the current step, the corresponding Step class will process the data of the step and increase the step by one.
 *
 * @author shadi
 */
class RegistrationController {

    private $request;
    private $view;
    private $step;

    public function __construct(Request $request, View $view) {
        $this->request = $request;
        $this->view = $view;
    }

    public function index() {
        $error = false;
        $session = $this->request->getSession();
        if ($session->get('step') === null) { // first request .. no 'step' session yet => render step1.html
            $session->set('step', '1');
        } else { // there is a 'step' session => the request must have data and need processing => create the right Step class and process the data.
            if ($this->request->request->get('submit') !== null) {
                $stepClass = "BasicRegistrationApp\Controller\Step" . $session->get('step'); // dynamic in case we need to add new steps.
                $this->step = new $stepClass($this->request);
                if ($this->step->process() === FALSE) {
                    $error = true;
                }
            }
        }
        if ($session->get('success') === null) { // registration has not finished yet => show the next step page.
            $stepNumer = $session->get('step');
            $html = $this->view->render('step' . $stepNumer . '.php', array("error" => $error));
            return new Response($html);
        } else { // when all steps finish successfully
            $response = new RedirectResponse('/success');
            return $response;
        }
    }

}
