<?php

namespace BasicRegistrationApp\Controller;

use Symfony\Component\HttpFoundation\Request;

/**
 * Description of Step2
 *
 * @author shadi
 */
class Step2  extends AbstractStep{

    protected $request;
    protected $fields = array('street', 'houseNumber', 'zipCode', 'city');

    function __construct(Request $request) {
        $this->request = $request;
    }

    public function process() {
        $session = $this->request->getSession();
        if ($this->fieldsHasData()) {
            foreach ($this->fields as $field) {
                $session->set($field, htmlspecialchars($this->request->request->get($field)));
            }
            $session->set('step', '3');
        } else {
            return false;
        }
    }
}
