<?php

/**
 * Description of View
 *
 * @author shadi
 */

namespace BasicRegistrationApp\View;

class View
{
   public function render($template, array $vars = array())
   {
      ob_start();
      extract($vars);
      require(dirname(__DIR__) . '/../templates/header.html');
      require(dirname(__DIR__) . '/../templates/' . $template);
      require(dirname(__DIR__) . '/../templates/footer.html');
      return ob_get_clean();
   }
}
