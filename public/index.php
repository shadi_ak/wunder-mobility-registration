<?php

declare(strict_types = 1);
require_once dirname(__DIR__) . '/vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\Dotenv\Exception\PathException;
use Symfony\Component\HttpFoundation\Response;
use BasicRegistrationApp\View\View;

$session = new Session();
$session->start();

$dotenv = new Dotenv();
try {
    $dotenv->load(dirname(__DIR__) . '/.env');
} catch (PathException $e) {
    echo $e->getMessage();
    die();
}

try {
    // setting up the routers
    $fileLocator = new FileLocator(dirname(__DIR__) . "/config");
    $loader = new YamlFileLoader($fileLocator);
    $routes = $loader->load('routes.yaml');
    $context = new RequestContext();
    $request = Request::createFromGlobals();
    if (!$request->hasSession()) {
        $request->setSession($session);
    }
    $context->fromRequest($request);
    $matcher = new UrlMatcher($routes, $context);
    $parameters = $matcher->match($context->getPathInfo());
    $controller = $parameters['_controller'];
    $controllerParts = explode("::", $controller);
    $view = new View();
    $controllerObject = new $controllerParts[0]($request, $view);
    $controllerMethod = $controllerParts[1];
    $response = $controllerObject->$controllerMethod();
    $response->send();
} catch (ResourceNotFoundException $e) {
    $response = new Response('<h2>Page not found!</h2> To register <a href="/registration">click here</a>', Response::HTTP_NOT_FOUND, ['content-type' => 'text/html']);
    $response->send();
}